module Main where
---------------------------------------------------------------------------
-- Xmonad config
---------------------------------------------------------------------------

import XMonad
import System.Exit
-- import XMonad.Hooks.SetWMName

-- toggle layouts better
-- XMonad.Layout.MultiToggle

-- layouts
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.Grid
import XMonad.Layout.Accordion
import XMonad.Layout.Circle
import XMonad.Layout.Cross
import XMonad.Layout.Dwindle
import XMonad.Layout.Roledex
import XMonad.Layout.TwoPane
import XMonad.Layout.ZoomRow
import XMonad.Layout.StackTile
import XMonad.Layout.SimpleFloat

-- lib to chance deffault keybindings
import qualified XMonad.StackSet as W

-- Allow programs to be spawned
import XMonad.Util.Run(spawnPipe)

-- Gaps
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps

-- TODO
import XMonad.Hooks.ManageDocks
import XMonad.Util.EZConfig(additionalKeys)

------------------------------------------------------------------------------
-- vars
------------------------------------------------------------------------------

myExtraWorkspaces = [(xK_0, "0"),(xK_minus, "tmp"),(xK_equal, "swap")]
myWorkspaces = ["1","2","3","4","5","6","7","8","9"] ++ (map snd myExtraWorkspaces)

myModMask       = mod1Mask
myTerminal      = "urxvt"
myBorderWidth   = 0

gw = 2
-- gwU = 34
gwU = 7
gwD = 7
gwL = 7
gwR = 7

------------------------------------------------------------------------------
-- Functions
------------------------------------------------------------------------------

myManageHook = composeAll
    [ className =? "Tor Browser" --> doFloat ]

------------------------------------------------------------------------------

myKeys = [
        -- window management
        ---- replace current window with the root window
          ((myModMask,                xK_e     ), windows W.swapMaster)
        , ((myModMask,                xK_q     ), kill)
        , ((myModMask,                xK_a     ), sendMessage FirstLayout)
        , ((myModMask,                xK_f     ), sendMessage NextLayout)
        , ((myModMask,                xK_o     ), sendMessage Rotate)
        , ((myModMask .|. shiftMask,  xK_n     ), sendMessage MoveNode)
        ---- sink floating window to tilling
        , ((myModMask,                xK_s     ), withFocused $ windows . W.sink)

        -- xmonad management
        , ((myModMask .|. shiftMask,  xK_q     ), io (exitWith ExitSuccess))
        , ((myModMask,                xK_r     ), spawn
        "xmonad --recompile && xmonad --restart"
        )

        -- lockscreen
        , ((myModMask,                xK_Delete), spawn
        "systemctl suspend && physlock -d"
        )

        -- launch user software
        , ((myModMask, xK_Return ), spawn "urxvt")
        , ((myModMask, xK_b      ), spawn "firefox")
        , ((myModMask, xK_t      ), spawn "tor-browser-en")
        , ((myModMask, xK_g      ), spawn "transmission-gtk")
        , ((myModMask, xK_u      ), spawn "~/.scripts/mus.sh")
        , ((myModMask, xK_p      ), spawn "keepmenu")
        , ((myModMask, xK_i      ), spawn "~/.scripts/wallpapers.sh")
        , ((myModMask, xK_d      ), spawn "~/.config/rofi/menu/search")
        , ((myModMask, xK_n      ), spawn "urxvt -bg 0 -e newsboat")
        , ((myModMask, xK_v      ), spawn "~/.scripts/string_handler.sh -i")
        , ((myModMask, xK_c      ), spawn "~/.scripts/string_handler.sh -p")

        -- Brightness
        , ((myModMask, 0x1008FF02), spawn "~/.scripts/backlight.sh -s 100")
        , ((myModMask, 0x1008FF03), spawn "~/.scripts/backlight.sh -s 0")
        , ((0,         0x1008FF02), spawn "~/.scripts/backlight.sh -i 10")
        , ((0,         0x1008FF03), spawn "~/.scripts/backlight.sh -i -10")
        , ((shiftMask, 0x1008FF02), spawn "~/.scripts/backlight.sh -i 2")
        , ((shiftMask, 0x1008FF03), spawn "~/.scripts/backlight.sh -i -2")

        -- Other special keys
        , ((myModMask, xK_o      ), spawn "~/.scripts/screenshot.sh")


        , ((0        , 0x1008FF07), spawn "xrandr --output HDMI-2 --mode 1920x1080")

        -- volume keys
        , ((0        , 0x1008FF12), spawn "amixer -D pulse set Master 1+ toggle")
        , ((0        , 0x1008FF11), spawn "amixer -q sset Master 1%-") 
        , ((0        , 0x1008FF13), spawn "amixer -q sset Master 1%+")
        , ((shiftMask, 0x1008FF11), spawn "amixer -q sset Master 5%-") 
        , ((shiftMask, 0x1008FF13), spawn "amixer -q sset Master 5%+")

        -- airow keys for media controlls
        -- Next, previeus, pause, quit
        , ((myModMask,                xK_Left  ), spawn
        "xdotool search --class mpv|xargs -I % xdotool key --window % shift+60"
        )
        , ((myModMask,                xK_Right ), spawn
        "xdotool search --class mpv|xargs -I % xdotool key --window % shift+94"
        )
        , ((myModMask,                xK_Up    ), spawn
        "xdotool search --class mpv|xargs -I % xdotool key --window % space"
        )
        , ((myModMask,                xK_Down  ), spawn
        "xdotool search --class mpv|xargs -I % xdotool key --window % q"
        )
	] ++ [
            ((myModMask, key), (windows $ W.greedyView ws))
            | (key,ws) <- myExtraWorkspaces
        ] ++ [
            ((myModMask .|. shiftMask, key), (windows $ W.shift ws))
            | (key,ws) <- myExtraWorkspaces
        ]

------------------------------------------------------------------------------

mySpacing = spacing gw
myGaps =   gaps [(U,gwU),(D,gwD),(L,gwL),(R,gwR)]

------------------------------------------------------------------------------

myLayout =  mySpacing (myGaps (
        GridRatio (4/3) )) |||
--        emptyBSP )) ||| 
--      simpleCross |||
--        tiled )) |||
--      Roledex |||
--      Circle |||
--      Dwindle R CW (2/2) (10/10) |||
--      Spiral L CW (2/2) (10/10) )) |||
--
--      -- Layout that puts non-focused windows 
--      -- in ribbons at the top and bottom of the screen.
--      Accordion |||

--      Grid |||
        Full 
    where
        -- default tiling algorithm partitions the screen into two panes
        tiled   = Tall nmaster delta ratio

        -- The default number of windows in the master pane
        nmaster = 1

        -- Default proportion of screen occupied by master pane
        ratio   = 1/2

        -- Percent of screen to increment by when resizing panes
        delta   = 3/100

------------------------------------------------------------------------------
-- main
------------------------------------------------------------------------------

main = do xmonad 
    $ defaultConfig
        { manageHook = manageDocks <+> myManageHook
                        <+> manageHook defaultConfig
        , workspaces = myWorkspaces
        , layoutHook = myLayout
        , terminal = myTerminal 
        , borderWidth = myBorderWidth
        , modMask = myModMask
        }

        -- Keybindings
        `additionalKeys` myKeys
------------------------------------------------------------------------------
