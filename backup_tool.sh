#!/bin/bash
# dotfile backup manager, Script to handle dotfiles with fzf

cd ~/.dotfiles/

add_dot_by_search(){
	dot=$(find $1 | fzf)
	new_line="$dot,"
	[ -f $dot ] && read -p "New location in repo dir: " v; new_line+="$v"
	grep "$dot" "dots.csv" || echo "$new_line" >> "dots.csv"
}

add_dot_by_pwd(){
	echo "Function not working yet"; exit

	read -p "Config pwd: " dot
	new_line=''
	grep "$dot" "dots.csv" || new_line+="$dot, "
	[ -f $dot ] && read -p "New location in repo dir: " v; new_line+="$v"
	echo "$new_line" #>> "dots.csv"
}

backup_dots(){
	cat dots.csv | while read dot
	do
		source_=$(echo "$dot" | awk -F, '{print $1}')
		new_dir=$(echo "$dot" | awk -F, '{print $2}')
		[ -d $source_ ] && cp -r "$source_" .
		[ -f $source_ ] && mkdir -p $new_dir && cp "$source_" "$new_dir"
	done
	ls ~/.dotfiles
	git -C ~/.dotfiles status
}

case "$1" in
	"") backup_dots;;
	"-n") add_dot_by_pwd && backup_dots;;
	"-s") add_dot_by_search $2 && backup_dots;;
	*) echo "Not a valid function";;
esac
