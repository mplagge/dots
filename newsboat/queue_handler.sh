#!/bin/bash
# Download queue file files generated with newsboat scripts

nb_dir="$HOME/.newsboat"
mkdir -p "$nb_dir"

download_youtube(){
	# Function to download queue files with youtube-dl
	queue_file=$1; type_=$2; yt_dl_flags=$3

	[ -s "$nb_dir/$queue_file" ] &&\
	youtube-dl $yt_dl_flags $(cat $nb_dir/$queue_file) -o "./$type_/%(title)s.%(ext)s"\
		&& > "$nb_dir/$queue_file"
	#echo $type_ && echo -e '\t\c' && ls "$nb_dir/$type_" | sed ':a;N;$!ba;s/\n/\n\t/g'
}

download_youtube queue_mus mus -x
download_youtube queue_vid vid --ignore-errors
download_youtube queue_cre cre -x
