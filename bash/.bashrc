#!/bin/bash

PATH=$PATH:~/.cabal/bin
. "${HOME}/.cache/wal/colors.sh"

set -o vi
PS1="\\W\e[1;34m » \e[0;37m"

# ------ Global variables ------#

# To import into bash script:
# source ~/.bashrc

MENU="$HOME/.config/rofi/menu/pipe"
BROWSER="firefox"
BROWSERSEARCH="firefox --search"
IMAGEVIEWER="sxiv"
TERMINAL="urxvt"
TERMINALCOM="urxvt -bg 0 -e" 

# ------ bash aliases ------#
	
# package management
	alias rsn='sudo pacman -Rsn'
	alias syu='yay'
	alias qdt='pacman -Qdt'
# user applications
	alias n='urxvt &'
	alias sc='sc-im *.sc'
	alias v="vim"
# diaplay
	alias hdmi='xrandr --output HDMI-2 --mode 1920x1080'
# manuals
	alias m="man"
	alias aw="awman"
# systemd
	alias systemctl="sudo systemctl"
# volume management
	alias lb="lsblk"
	alias mount="sudo mount"
	alias umount="sudo umount"
	alias amnt="udiskie"
	alias umnt="udiskie-umount"
# shortcuts directories
	alias ex="cd ~/doc/ex && ls"
	alias hr="cd ~/doc/sch/hr3/"
	alias emb="cd ~/doc/sch/hr3/emb && ls"
	alias ml="cd ~/doc/sch/hr3/ml && ls"
	alias mov="cd ~/dow/mov && ls"
	alias dow="cd ~/dow && ls"
	alias tst="cd ~/tst && ls"
	alias doc="cd ~/doc && ls"
	alias lea="cd ~/doc/lea && ls"
	alias bas="cd ~/bas && ls"
	alias tem="cd ~/doc/tem && ls"
# simplefied standaard commands
	alias c="clear"
	alias s="sudo"
	alias q="exit"
	alias qq="exit"
	alias qqq="exit"
	alias Q="exit"
	alias QQ="exit"
	alias g="grep"
	alias la='ls -a --color=auto'
	alias l='ls --color=auto'
	alias ls='ls --color=auto'
	alias sl='ls --color=auto'
	alias ..="cd .."
	alias ...="cd ../../"
	alias ....="cd ../../../"
	alias .....="cd ../../../../"
# config files
	alias setx="xrdb ~/.Xresources"
	alias sxhkdrc="vim ~/.config/sxhkd/sxhkdrc"
	alias bspwmrc="vim ~/.config/bspwm/bspwmrc"
	alias bashrc="vim ~/.bashrc"
	alias xinitrc="vim ~/.xinitrc"
	alias i3rc="vim ~/.config/i3/config"
	alias qtilerc="vim ~/.config/qtile/config.py"
	alias vimrc="vim ~/.vimrc"
	alias xmonadrc="vim ~/.xmonad/xmonad.hs"
	alias xr="xmonad --recompile"
# music
	alias mus="sh ~/.scripts/mus.sh"
# newsboat
	alias nbrc="vim ~/.config/newsboat/config"
	alias nbu="vim ~/.config/newsboat/urls"
	alias nbd="youtube-dl --rm-cache-dir && ~/.config/newsboat/queue_handler.sh"
	alias nb="newsboat"
# Languages
	# Haskell
	alias vh="vim *.hs"
	alias ghc="ghc -dynamic"
	# adoc
	alias va="vim *.adoc"
	# LaTeX
	alias vx="vim *.tex"
	alias x="zathura"
	alias xx="zathura *.pdf"
	alias tx="pdflatex *.tex"
	# java
	alias j="java"
	alias jc="javac *.java"
	alias vj="vim *.java"
	alias jr="java App"
	alias jep="~/.scripts/jep.sh"
	# haskell
	alias h="ghc *.hs && ./*"
	alias hi="ghci *.hs"
	# python
	alias p="python"
# Permissions
	alias mx="chmod +x *"
# file controll
	alias fr="~/.scripts/fr.sh"
#Platformio
	alias pm='pio device monitor'
	alias pu='pio run && pio device monitor'
	alias pl='pio run && serialplot'

# ------ commands reminders ------#

# Arduino ports
# stty -F /dev/ttyACM0 cs8 9600 ignbrk -brkint -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts
# screen /dev/ttyACM0 9600

# smartcard
# systemctl start pcscd && pcsc_scan >> log


