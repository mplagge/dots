#!/bin/bash
# script to execute a bspwm command acording to the cursor location

xMax=$(xdotool getdisplaygeometry | awk -F" " '{print $1}')
xMid=$(echo "$xMax / 2" | bc)

xCurrent=$(xdotool getmouselocation | awk -F" |:" '{print $2}')

echo $xCurrent
echo $xMid

if [ $xCurrent -lt $xMid ]; then
	bspc desktop -f prev
else
	bspc desktop -f next
fi
