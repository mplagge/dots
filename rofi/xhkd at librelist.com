SXHKD(1)                                        Sxhkd Manual                                       SXHKD(1)

NNAAMMEE
       sxhkd - Simple X hotkey daemon

SSYYNNOOPPSSIISS
       ssxxhhkkdd [_O_P_T_I_O_N_S] [_E_X_T_R_A___C_O_N_F_I_G ...]

DDEESSCCRRIIPPTTIIOONN
       sxhkd is a simple X hotkey daemon with a powerful and compact configuration syntax.

OOPPTTIIOONNSS
       --hh
           Print the synopsis to standard output and exit.

       --vv
           Print the version information to standard output and exit.

       --mm _C_O_U_N_T
           Handle the first _C_O_U_N_T mapping notify events.

       --tt _T_I_M_E_O_U_T
           Timeout in seconds for the recording of chord chains.

       --cc _C_O_N_F_I_G___F_I_L_E
           Read the main configuration from the given file.

       --rr _R_E_D_I_R___F_I_L_E
           Redirect the commands output to the given file.

       --ss _S_T_A_T_U_S___F_I_F_O
           Output status information to the given FIFO.

       --aa _A_B_O_R_T___K_E_Y_S_Y_M
           Name of the keysym used for aborting chord chains.

BBEEHHAAVVIIOORR
       ssxxhhkkdd is a daemon that listens to keyboard events and execute commands.

       It reads its configuration file from $$XXDDGG__CCOONNFFIIGG__HHOOMMEE//ssxxhhkkdd//ssxxhhkkddrrcc by default, or from the given
       file if the --cc option is used.

       Additional configuration files can be passed as arguments.

       If ssxxhhkkdd receives a _S_I_G_U_S_R_1 (resp. _S_I_G_U_S_R_2) signal, it will reload its configuration file (resp.
       toggle the grabbing state of all its bindings).

       The commands are executed via SSHHEELLLL _-_c CCOOMMMMAANNDD (hence you can use environment variables).

       SSHHEELLLL will be the content of the first defined environment variable in the following list:
       SSXXHHKKDD__SSHHEELLLL, SSHHEELLLL.

       If you have a non-QWERTY keyboard or a non-standard layout configuration, you should provide a _C_O_U_N_T
       of _1 to the --mm option or _-_1 (interpreted as infinity) if you constantly switch from one layout to
       the other (ssxxhhkkdd ignores all mapping notify events by default because the majority of those events
       are pointless).

CCOONNFFIIGGUURRAATTIIOONN
       Each line of the configuration file is interpreted as so:

       +o   If it is empty or starts with #, it is ignored.

       +o   If it starts with a space, it is read as a command.

       +o   Otherwise, it is read as a hotkey.

       General syntax:

           HOTKEY
               [;]COMMAND

           HOTKEY      := CHORD_1 ; CHORD_2 ; ... ; CHORD_n
           CHORD_i     := [MODIFIERS_i +] [~][@]KEYSYM_i
           MODIFIERS_i := MODIFIER_i1 + MODIFIER_i2 + ... + MODIFIER_ik

       The valid modifier names are: _s_u_p_e_r, _h_y_p_e_r, _m_e_t_a, _a_l_t, _c_o_n_t_r_o_l, _c_t_r_l, _s_h_i_f_t, _m_o_d_e___s_w_i_t_c_h, _l_o_c_k,
       _m_o_d_1, _m_o_d_2, _m_o_d_3, _m_o_d_4, _m_o_d_5 and _a_n_y.

       The keysym names are given by the output of xxeevv.

       Hotkeys and commands can be spread across multiple lines by ending each partial line with a
       backslash character.

       When multiple chords are separated by semicolons, the hotkey is a chord chain: the command will only
       be executed after receiving each chord of the chain in consecutive order.

       The colon character can be used instead of the semicolon to indicate that the chord chain shall not
       be aborted when the chain tail is reached.

       If a command starts with a semicolon, it will be executed synchronously, otherwise asynchronously.

       The _E_s_c_a_p_e key can be used to abort a chord chain.

       If @@ is added at the beginning of the keysym, the command will be run on key release events,
       otherwise on key press events.

       If ~~ is added at the beginning of the keysym, the captured event will be replayed for the other
       clients.

       Pointer hotkeys can be defined by using one of the following special keysym names: _b_u_t_t_o_n_1, _b_u_t_t_o_n_2,
       _b_u_t_t_o_n_3, ..., _b_u_t_t_o_n_2_4.

       The hotkey and the command may contain sequences of the form _{_S_T_R_I_N_G___1_,_._._._,_S_T_R_I_N_G___N_}.

       In addition, the sequences can contain ranges of the form _A-_Z where _A and _Z are alphanumeric
       characters.

       The underscore character represents an empty sequence element.

AAUUTTHHOORR
       Bastien Dejean <nihilhill at gmail.com>

MMAAIILLIINNGG LLIISSTT
       sxhkd at librelist.com

Sxhkd 0.6.0                                      02/13/2019                                        SXHKD(1)
