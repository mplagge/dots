""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Genaral setup
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ! Using plug
" Plug 'dylanaraps/wal.vim'
" colorscheme wal

" Enable_mouse_clicking:
	set mouse=a
" Enable_clipboard:
	set clipboard=unnamedplus
" Disable_airow_keys:
	noremap <Up> <NOP>
	noremap <Down> <NOP>
	noremap <Left> <NOP>
	noremap <Right> <NOP>
" Limit_line_length:
	highlight ColorColumn ctermbg=red
	call matchadd('ColorColumn', '\%100v', 100)
" Enable_folding:
	set foldmethod=indent
	set foldlevel=99
	nnoremap <space> za
" Line_numbering:
	set number
	set relativenumber
" Colorschemes:
	syntax on
	:colorscheme ron 
" Indent:
	set smartindent
" Opening_exports:
	map <Tab> :!zathura *.pdf & <Enter><Enter>
" Spelling:
	set spelllang=en_us,nl
	map <F3> :setlocal spell! <CR>
	autocmd BufNewFile,BufRead *.tex set spell
	autocmd BufNewFile,BufRead *.rmd set spell
	autocmd BufNewFile,BufRead *.acod set spell
" Compiling:
	autocmd Filetype tex map <Enter> :w \| !pdflatex *.tex<Enter>
	autocmd Filetype rmd map <Enter> :w \| !echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter>
	autocmd Filetype asciidoc map <Enter> :w \| !asciidoc % <Enter>
	autocmd Filetype cpp map <Enter> :w \| !g++ *.cpp<.tex<Enter>
	autocmd Filetype c map <Enter> :w \| !gcc *.c && ./*
	autocmd Filetype haskell set tabstop=4
	autocmd Filetype haskell set shiftwidth=4
	autocmd Filetype haskell set softtabstop=4
	autocmd Filetype haskell set expandtab
" Navigation_guides:
	" <++> = next empty field
	inoremap ;n <Esc>/<++><Enter>"_c4l
	map ;n <Esc>/<++><Enter>"_c4l
	inoremap ;d <Esc>/<++><Enter>"_c4l<Esc>dd
" Set_filetype:
	filetype plugin on
	autocmd BufRead,BufNewFile *.tex set filetype=tex
	autocmd BufRead,BufNewFile *.py set filetype=python
	autocmd BufRead,BufNewFile *.php,*.html set filetype=web
	autocmd BufRead,BufNewFile *.php,*.html set syntax=php
	autocmd BufRead,BufNewFile *.cpp,*.c set syntax=c
	autocmd BufRead,BufNewFile *.hs set syntax=haskell
" Exit_insert_mode:
	inoremap ;a <Esc>
	inoremap ;w <Esc>:wq<Enter>
	inoremap ;s <Esc>:w<Enter>
	map ;w :wq<Enter>
	map ;s :w<Enter>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Snippets
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" PROGRAMMING
" ;e = comment
" ;q = output
" ;d = datatypes
" ;f = funtions
" ;t = template (setup)
" ;c = conditions
" ;cs = switch case
" ;k = classes

" MARKUP
" ;p = special snippet

"------ Programming/Scripting languages ------"

" Java
	" commends "
	autocmd Filetype java inoremap ;ea <Esc>0i/*<Esc>$a*/<Esc>
	autocmd Filetype java inoremap ;eq <Esc>0xx<Esc>$xx<Esc>
	autocmd Filetype java inoremap ;qf System.out.println();<Esc>hi
	autocmd Filetype java inoremap ;qn System.out.println();<Esc>hi
	autocmd Filetype java inoremap ;qd System.out.println("");<Esc>2hi
	autocmd Filetype java inoremap ;t class {<Enter>public static void main(String args[]){<Enter><++><Enter><Backspace>}<Enter><Backspace>}<Esc>4kwi
	autocmd Filetype java inoremap ;fv public static void (){<Enter><++><Enter><Backspace>}<Esc>2k3wi
	" Fuctions "
	autocmd Filetype java inoremap ;fb public static bool (<++>){<Enter><++><Enter>return <++>;<Enter><Backspace>}<Esc>3k3wi
	autocmd Filetype java inoremap ;fi public static int (<++>){<Enter><++><Enter>return <++>;<Enter><Backspace>}<Esc>3k3wi
	autocmd Filetype java inoremap ;fc public static char (<++>){<Enter><++><Enter>rkturn <++>;<Enter><Backspace>}<Esc>3k3wi
	autocmd Filetype java inoremap ;fd public static double (<++>){<Enter><++><Enter>return <++>;<Enter><Backspace>}<Esc>3k3wi
	autocmd Filetype java inoremap ;ff public static float (<++>){<Enter><++><Enter>return <++>;<Enter><Backspace>}<Esc>3k3wi
	autocmd Filetype java inoremap ;fs public static string (<++>){<Enter><++><Enter>return <++>;<Enter><Backspace>}<Esc>3k3wi
	autocmd Filetype java inoremap ;fv public static void (){<Enter><++><Enter><Backspace>}<Esc>2k3wi
" C
	" commends "
	autocmd Filetype c inoremap ;t #include <stdlib.h><Enter>#include <stdio.h><Enter>int main(){<Enter><Enter><Backspace>}<Esc>ki<Tab>
	autocmd Filetype c inoremap ;ea <Esc>0i/*<Esc>$a*/<Esc>
	autocmd Filetype c inoremap ;eq <Esc>0xx<Esc>$xx<Esc>
" C++
" 	" template
	autocmd Filetype cpp inoremap ;t #include <iostream><Enter>#include <string><Enter><Enter>using namespace std;<Enter><Enter>int main(int argc, char** argv){<Enter><Enter>return 0;<Enter><Backspace>}<Esc>2ki<Tab>
	" output (q(uoting))"
	autocmd Filetype cpp inoremap ;qf cout << ;<Esc>i
	autocmd Filetype cpp inoremap ;qn cout <<  << "\n";<Esc>4bhi
	autocmd Filetype cpp inoremap ;qd cout << "" << "\n";<Esc>5ba
	autocmd Filetype cpp inoremap ;qv cout << "" << <++> << "\n";<Esc>7ba
	" datatypes (d) "
	autocmd Filetype cpp inoremap ;di int  = <++>;<Esc>2bhi
	autocmd Filetype cpp inoremap ;db bool  = <++>;<Esc>2bhi
	autocmd Filetype cpp inoremap ;df float  = <++>;<Esc>2bhi
	autocmd Filetype cpp inoremap ;dd double  = <++>;<Esc>2bhi
	autocmd Filetype cpp inoremap ;ds string  = "<++>";<Esc>2bhi
	autocmd Filetype cpp inoremap ;dh char  = "<++>";<Esc>2bhi
	autocmd Filetype cpp inoremap ;dcb const bool  = <++>;<Esc>2bhi
	autocmd Filetype cpp inoremap ;dci const int  = <++>;<Esc>2bhi
	autocmd Filetype cpp inoremap ;dcf const float  = <++>;<Esc>2bhi
	autocmd Filetype cpp inoremap ;dcd const double  = <++>;<Esc>2bhi
	autocmd Filetype cpp inoremap ;dcs const string  = "<++>";<Esc>2bhi
	autocmd Filetype cpp inoremap ;dch const char  = "<++>";<Esc>2bhi
	" functions (f) "
	autocmd Filetype cpp inoremap ;fb bool (<++>){<Enter><++><Enter>return <++>;<Enter><Backspace>}<Esc>3kwi
	autocmd Filetype cpp inoremap ;fi int (<++>){<Enter><++><Enter>return <++>;<Enter><Backspace>}<Esc>3kwi
	autocmd Filetype cpp inoremap ;fc char (<++>){<Enter><++><Enter>rkturn <++>;<Enter><Backspace>}<Esc>3kwi
	autocmd Filetype cpp inoremap ;fd double (<++>){<Enter><++><Enter>return <++>;<Enter><Backspace>}<Esc>3kwi
	autocmd Filetype cpp inoremap ;ff float (<++>){<Enter><++><Enter>return <++>;<Enter><Backspace>}<Esc>3kwi
	autocmd Filetype cpp inoremap ;fs string (<++>){<Enter><++><Enter>return <++>;<Enter><Backspace>}<Esc>3kwi
	autocmd Filetype cpp inoremap ;fv void (<++>){<Enter><++><Enter><Backspace>}<Esc>3k2wi
	" classes (l) "
	autocmd Filetype cpp inoremap ;lc class {<Enter>private:<Enter><Tab><++><Enter><Backspace>public:<Enter><Tab><++><Enter>~<++>();<Enter><Backspace><Backspace>};<Enter><++><Esc>7kwi
	" conditions (c) "
	""" if
	autocmd Filetype cpp inoremap ;cff if(){<Enter><++><Enter><Backspace>}<++><Esc>2khi
	autocmd Filetype cpp inoremap ;cfo if()<Enter><++><Enter><Backspace><++><Esc>2ki
	autocmd Filetype cpp inoremap ;cfi if() <++>;<Esc>2ba
	""" else if
	autocmd Filetype cpp inoremap ;cnf else if(){<Enter><++><Enter><Backspace>}<++><Esc>2k2wa
	autocmd Filetype cpp inoremap ;cno else if()<Enter><++><Enter><Backspace><++><Esc>2k2wa
	autocmd Filetype cpp inoremap ;cni else if() <++>;<Esc>2ba
	""" else
	autocmd Filetype cpp inoremap ;cef else{<Enter><Tab><Backspace><Enter><Backspace>}<Esc>ka
	autocmd Filetype cpp inoremap ;ceo else<Enter><Tab><Backspace><Enter><Backspace><Esc>2ka
	autocmd Filetype cpp inoremap ;cei else ;<Esc>i
	""" switch
	autocmd Filetype cpp inoremap ;cs switch(){<Enter>case <++>:<Enter><Tab><++><Enter>break;<Enter><Backspace>case <++>:<Enter><Tab><++><Enter>break;<Enter><Backspace>default:<Enter><Tab><++><Enter><Backspace><Backspace>}<Esc>9kwa
	""" variable = (condition) ? true : false
	" loops (l) "
	autocmd Filetype cpp inoremap ;li for(int i = 0;; i++){<Enter><++><Enter><Backspace>}<Enter><++><Esc>3k5wa<Space>
	autocmd Filetype cpp inoremap ;lj for(int j = 0;; j++){<Enter><++><Enter><Backspace>}<Enter><++><Esc>3k5wa<Space>
	autocmd Filetype cpp inoremap ;lk for(int k = 0;; k++){<Enter><++><Enter><Backspace>}<Enter><++><Esc>3k5wa<Space>
	autocmd Filetype cpp inoremap ;lw while(){<Enter><++><Enter><Backspace>}<Enter><++><Esc>3kwa
" Python
	autocmd Filetype python inoremap ;t #!/usr/bin/env python<Enter><Enter>def main():<Enter><Tab><Enter><Backspace><Enter>if __name__ == '__main__':<Enter>main()<Esc>3ka
	autocmd Filetype python inoremap ;f def ():<Enter><Tab><++><Esc>kbi
	autocmd Filetype python inoremap ;qd print('')<Esc>hi
	autocmd Filetype python inoremap ;qf print()<Esc>i

" Shell
	autocmd Filetype sh inoremap ;t #!/bin/bash<Enter># <Enter><Enter><++><Esc>2ka

"------ Typesetting languages (Excluding html,css) ------"

" LaTeX: 
	" documentclasses (s) "
	autocmd Filetype tex inoremap ;ta \documentclass{article}<Enter><Enter>\title{}<Enter>\author{<++>}<Enter><Enter>\begin{document}<Enter><Enter>\maketitle<Enter><Enter><++><Enter><Enter>\end{document}<Esc>9ki
	autocmd Filetype tex inoremap ;tr \documentclass{report}<Enter><Enter>\title{}<Enter>\author{<++>}<Enter><Enter>\begin{document}<Enter><Enter>\maketitle<Enter><Enter><++><Enter><Enter>\end{document}<Esc>9ki
	autocmd Filetype tex inoremap ;sb \documentclass{book}<Enter><Enter>\title{}<Enter>\author{<++>}<Enter><Enter>\begin{document}<Enter><Enter>\maketitle<Enter><Enter><++><Enter><Enter>\end{document}<Esc>9ki
	" text formatting"
	autocmd Filetype tex inoremap ;b \textbf{} <++><Esc>2ba
	autocmd Filetype tex inoremap ;i \textit{} <++><Esc>2ba
	autocmd Filetype tex inoremap ;t \texttt{} <++><Esc>2ba
	" lists
	autocmd Filetype tex inoremap ;li \begin{itemize}<Enter><Tab>\item<Enter><Backspace>\end{itemize}<Esc>ka<Space>
	autocmd Filetype tex inoremap ;ln \begin{enumerate}<Enter><Tab>\item<Enter><Backspace>\end{enumerate}<Esc>ka<Space>
	autocmd Filetype tex inoremap ;ld \begin{description}<Enter><Tab>\item<Enter><Backspace>\end{description}<Esc>ka<Space>
	" document structure "
	autocmd Filetype tex inoremap ;ch \chapter{}\label{ch:<++>}<Enter><Enter><++><Esc>2kwa
	autocmd Filetype tex inoremap ;sc \section{}\label{sec:<++>}<Enter><Enter><++><Esc>2kwa
	autocmd Filetype tex inoremap ;ssc \subsection{}\label{ssec:<++>}<Enter><Enter><++><Esc>2kwa
	autocmd Filetype tex inoremap ;sssc \subsubsection{}\label{sssec:<++>}<Enter><Enter><++><Esc>2kwa
	autocmd Filetype tex inoremap ;p \paragraph{}\label{p:<++>}<Enter><Enter><++><Esc>2kwa
	autocmd Filetype tex inoremap ;sp \subparagraph{}\label{sp:<++>}<Enter><Enter><++><Esc>2kwa
	" parts "
	autocmd Filetype tex inoremap ;pa \begin{abstract}<Enter><Enter>\end{abstract}<Enter><Esc>2ki
	" TODO "
	autocmd Filetype tex inoremap ;fig \begin{figure}<Enter><Tab>\caption{}<Enter>\centering<Enter><Tab>\includegraphics[width=0.5\textwidth]{<++>}<Enter><Backspace><Backspace>\end{figure}<Enter><Enter><++><Esc>5k3wa
	autocmd Filetype tex inoremap ;up \usepackage{}<Enter><++><Esc>kwa
	autocmd Filetype tex inoremap ;toc \tableofcontents<Enter><Enter>
	autocmd Filetype tex inoremap ;lot \listoftables<Enter><Enter>
	autocmd Filetype tex inoremap ;lof \listoffigures<Enter><Enter>

	function! AddItem()
	  let [end_lnum, end_col] = searchpairpos('\\begin{', '', '\\end{', 'nW')
	  if match(getline(end_lnum), '\(itemize\|enumerate\|description\)') != -1
	    return "\\item "
	  else
	    return ""
	  endif
	endfunction
	inoremap <expr><buffer> <CR> getline('.') =~ '\item $' 
	  \ ? '<c-w><c-w>' 
	  \ : (col(".") < col("$") ? '<CR>' : '<CR>'.AddItem() )
	nnoremap <expr><buffer> o "o".AddItem()
	nnoremap <expr><buffer> O "O".AddItem()

"------ Web programming ------"

" HTML
	autocmd Filetype html inoremap ;tem <!DOCTYPE html><Enter><html><Enter><Tab><head><Enter><Tab><title></title><Enter><++><Enter><Backspace></head><Enter><body><Enter><Tab><++><Enter><Backspace></body><Enter><Backspace></html><Esc>/<title><Enter>3wba
" WEB (php and javascript extend on these)
	autocmd Filetype php inoremap ;fo <form action="" method="<++>"><Enter><tab><input type="<++>" name="<++>"><Enter><input type="submit"><Enter><Backspace></form><Esc>/action=""<Enter>wla
	autocmd Filetype web inoremap ;hr <a href=""><++></a><++><Esc>3bla
	autocmd Filetype web inoremap ;ii <em></em><Space><++><Esc>FeT>i
	autocmd Filetype web inoremap ;bb <b></b><Space><++><Esc>FbT>i
	autocmd Filetype web inoremap ;qo <q></q><Space><++><Esc>FqT>i
	autocmd Filetype web inoremap ;hc <!----><Enter><Enter><++><Esc>2ka
	autocmd Filetype web inoremap ;h1 <h1></h1><Enter><Enter><++><Esc>2ka
	autocmd Filetype web inoremap ;h2 <h2></h2><Enter><Enter><++><Esc>2ka
	autocmd Filetype web inoremap ;h3 <h3></h3><Enter><Enter><++><Esc>2ka
	autocmd Filetype web inoremap ;h4 <h4></h4><Enter><Enter><++><Esc>2ka
	autocmd Filetype web inoremap ;pp <p></p><Enter><Enter><++><Esc>2kha
	autocmd Filetype web inoremap ;im <img src="" width="<++>" height="<++>" alt="<++>"><Enter><Enter><++><Esc>2k2wla
	autocmd Filetype web inoremap ;br <br>
	autocmd Filetype web inoremap ;hr <hr>
	autocmd Filetype web inoremap ;cs <link rel="stylesheet" href="styles.css"><Enter><++><Esc>k7w
	" <hr>
	" <br>
" CSS (External):
	autocmd Filetype css inoremap ;tem 
" JAVASCRIPT:
	autocmd Filetype web inoremap ;sc <script><Enter><Tab><Enter><Backspace></script><Esc>ka
" PHP:
	autocmd Filetype php inoremap ;tem <!DOCTYPE html><Enter><html><Enter><Tab><head><Enter><Tab><title></title><Enter><++><Enter><Backspace></head><Enter><body><Enter><Tab><?php<Enter><Tab><++><Enter><Backspace>?><Enter><Backspace></body><Enter><Backspace></html><Esc>/<title><Enter>2wba
	autocmd Filetype php inoremap ;cl class {<Enter><++><Enter>function __construct(<++>){<Enter><++><Enter><Backspace>}<Enter><Backspace>}<Enter><++><Esc>6kwi
	autocmd Filetype php inoremap ;fu function (<++>){<Enter><++><Enter><Backspace>}<Esc>2kea<Space>
	autocmd Filetype php inoremap ;if if(){<Enter><++><Enter><Backspace>}<Enter><++><Esc>3ki
	autocmd Filetype php inoremap ;ec echo();<Enter><++><Esc>k2li
	autocmd Filetype php inoremap ;eq echo("");<Enter><++><Esc>k3li
	autocmd Filetype php inoremap ;fu function (<++>){<Enter><++><Enter><Backspace>}<Enter><++><Esc>3kwi
