#!/bin/bash
# fuzzy remove
# script to remove a file via fzf

rm -i "$(ls | fzf)"
