#!/bin/bash
# Script to send notifications when battery is discharging

while true; do
	# if a battery discharges grep it

	msg="$(acpi | grep Discharging)" 

	# if discharged bettery not grepped sleep. else format msg end send it as notification
	[ -z "$msg" ] && sleep 60 \
		|| dunstify -a "bat" "$(echo $msg |\
			 awk -F' |,|%|:' '{print "BAT" $2 " " $6 "% " $9 ":" $10 "h"}')"\
			 ; sleep 60
done
