#!/bin/bash
# java, easier programming

full(){
	mkdir -p byt
	javac *.java && \
		 java $(echo $@ | awk -F. '{print $1}')
	mv *.class byt
}
run(){
	cd byt && java Main
}

help_message="\
	\n
	jep (Java, easy programming) \n
	\n
	\t Compile and run:\n
	\t \t./jep.sh MyMainFile.java\n
	\t \n
	\t Other options:\n
	\t \t-r \t\t  Run last compiled binary.\n
	\t \t-h/--help \t Display this message.\n
"

error_message="Invalid usage. Try 'jep -h' for more information."

case $1 in 
	*.java) full $1;;
	"-r") run $1;;
	"-h" | "--help") echo -e $help_message;;
	*) echo -e $error_message;;
esac
