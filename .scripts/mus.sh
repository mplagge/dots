#!/bin/bash
# script to launch music "modes" in mpv via dmenu

source ~/.bashrc

mode=$(echo -e "dir\nalb\none\nrm" | $MENU)

case "$mode" in
	"dir")  # Play a directory in a random order
		mpv --force-window --contrast=-95 --shuffle\
			"$(find ~/mus/ -type d | $MENU)"
		;;
	"alb")  # Play a directory in chronological order
		mpv --force-window --contrast=-95\
			"$(find ~/mus/ -type d | $MENU)"
		;;
	"one")  # Play one song
		mpv --force-window --contrast=-95\
			"$(find ~/mus/**/* -name '*' | $MENU)"
		;;

	"rm")   # Remove a song/directory
		remove=$(find ~/mus/**/* -name '*' | $MENU)
		check=$(echo -e "remove $remove\ny\nn" | $MENU)
		if [ "$check" = "y" -o "check" = "yes" ]; then
			rm -r "$remove"
		fi
		;;
esac

