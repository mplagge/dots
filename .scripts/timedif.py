#!/bin/python
import sys
from datetime import datetime

def main(argv):
    if len(argv) > 1:
        s1 = argv[1]
        s2 = argv[2]
        FMT = '%H:%M'
        tdelta = datetime.strptime(s2, FMT) - datetime.strptime(s1, FMT)
        print(tdelta)
    else:
        print("...")

if __name__ == '__main__':
    main(sys.argv)
