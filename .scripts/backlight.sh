#!/bin/bash
# script for changing brightness and redshift in X

refresh_i3blocks(){
	pkill -RTMIN+9 i3blocks
	pkill -RTMIN+11 i3blocks
}
check_dependencies(){
	for i in $@; do
		if ! [ "$(command -v $i)" ]; then
			missing_dependencies+="$i "
		fi
	done
	if [ -n "$missing_dependencies" ]; then
		echo "program(s) not installed: $missing_dependencies"
		exit
	fi
}
check_parameter(){
	numbers='^[0-9,-]+$'
	if [[ ! $1 =~ $numbers ]] || [ ! $1 -ge -100 ] || [ ! $1 -le 100 ]; then
		echo 'please enter a value from 1-100';	exit
	fi
}
increse_light(){
	light_value=$(echo "$(light) + $1" | bc | awk '{if($1 < 100) print $1; else print "100"}')
	echo "brightness: $light_value"
	light -S $light_value


	red_value=$(echo "$light_value*650/15+3500-33" | bc)
	echo "red shift:  $red_value"
	redshift -P -O $red_value > /dev/null
}
set_light(){
	bri=$1
	red=$(($1*650/15+3500-33))
	redshift -P -O $red > /dev/null
	light -S $bri 
	echo "brightness: $bri"
	echo "red shift:  $red"
}
print_help(){
	help_message="\
		what you can do:\n
		\t-i  increse/decrese backlight by a value (-100 to 100)\n
		\t-s  set backlight value (0 to 100)\n
		\t-h  print this message\n"
	echo -e $help_message
}

check_dependencies \
	redshift light bc
if [ "$1" == "-i" -o "$1" == "-s" ]; then
	check_parameter $2
fi

case "$1" in
	"-i") increse_light $2;;
	"-s") set_light $2;;
	"-h") print_help;;
	*) echo "Not a valid function";;
esac
